__version__ = '1.0.7'
from flask import Blueprint

BLUEPRINT_NAME = 'adminlte'
URL_PREFIX = '/adminlte3'

blueprint = Blueprint(BLUEPRINT_NAME,
                        __name__,
                        template_folder='templates',
                        static_folder='static')

class AdminLTE3(object):
    def __init__(self, app=None):
        if app is not None:
            self.init_app(app)

    def init_app(self, app):
        if not hasattr(app, 'extensions'):
            app.extensions = {}
        app.extensions['adminlte'] = self

        app.config.setdefault('FLASK_ADMINLTE3_BLUEPRINT_NAME', BLUEPRINT_NAME)
        app.config.setdefault('FLASK_ADMINLTE3_URL_PREFIX',URL_PREFIX)


        app.register_blueprint(blueprint,url_prefix=app.config['FLASK_ADMINLTE3_URL_PREFIX'])
